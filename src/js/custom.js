$(document).ready(function () {


    // jQuery.scrollSpeed(50, 1200);


    // main variables

    var wWidth = $(window).width();

    // end


    // STICKY NAV

    var stickyNav = function () {

        var wWidth = $(window).width();
        var wScrollTop = $(window).scrollTop();

        if(wScrollTop > 0 && wWidth > 767) {

            $('.navbar').addClass('sticky');

        } else {

            $('.navbar:not(.shop-nav-bg)').removeClass('sticky');

        }

        if(wScrollTop > 0 && wWidth < 769) {

            $('.m-navbar').addClass('sticky');

        } else {

            $('.m-navbar').removeClass('sticky');

        }

    };

    stickyNav();

    $(window).scroll(function () {

        stickyNav();

    });

    // END STICKY NAV


    // m-navbar popup

    $('.m-navbar__toggle').click(function () {
        $('.m-navbar__popup').fadeIn(300);
    });

    $('.m-navbar__close-popup, .m-navbar .cta-btn-login').click(function () {
       $('.m-navbar__popup').fadeOut(300);
    });

    // end


    // success form field

    $('input').change(function () {
        if ($(this).parent().hasClass('success-field')) {
            $(this).parent().append("<img src='img/svg/development/icon_development_check_green.svg'>")
        }
    });

    // end


    // checkboxes

    $('input[type=checkbox]').click(function(){
        $(this).parent().toggleClass('checked');
    });

    // end


    // home story column height

    var equalColumnHeight = function () {

        var wWidth = $(window).width();

        if (wWidth>1200) {

            var storyTextHeight = $('.home__story__row .home__story__column--text').height() + 210;

            $('.home__story__column--image').height(storyTextHeight);

        } else {



            $('.home__story__column--image').height(553);
        }

    };

    equalColumnHeight();

    $(window).resize(function () {
        equalColumnHeight();
    });

    // end


    // home commercial mobile select

    var takeContentTab = function () {


        $('.home__commercial__tabs li a').click(function () {

            var tabTitle = $(this).text();

            var tabImage = $('img', this).attr('src');

            $('.home__commercial__m-select img').attr('src', tabImage);

            $('.home__commercial__m-select b').text(tabTitle);

        });

    };

    takeContentTab();

    var customSelect = function () {

        var wWidth = $(window).width();

        if (wWidth < 993) {

            $('.home__commercial__m-select').click(function () {

                $('.home__commercial__tabs').show();

            });

            $('.home__commercial__tabs li a').click(function () {

                $('.home__commercial__tabs').hide();

            });

        } else {

            $('.home__commercial__tabs').show();

            $('.home__commercial__tabs').css({
                'display' : 'flex'
            });

            $('.home__commercial__tabs li a').click(function () {

                $('.home__commercial__tabs').show();

            });

        }

    };

    var selectOpened = false;

    $('.home__commercial__m-select').click(function (e) {

        e.preventDefault();

        if (!selectOpened) {

            $('.home__commercial__tabs').show();

            setTimeout(function () {
                selectOpened = true;
            });

        }


    });

    $('.home').click(function () {

        if (selectOpened) {

            $('.home__commercial__tabs').hide();

            selectOpened = false;
        }

    });


    customSelect();

    $(window).resize(function () {
        customSelect();
    });

    // end


    // shop page sidebar

    $('.shop-sidebar__list-item a').click(function (e) {

        e.preventDefault();

        $(this).parent().parent().find('.active').removeClass('active');

        $(this).parent().addClass('active');

    });

    // end


    // shop-product page form

    var countValue = $('.shop-product__count-input').val();

    var sumValue = $('.shop-product__product-sum').val();

    $('.shop-product__count-btn--minus').click(function () {

        if (countValue>0) {

            countValue--;

            $('.shop-product__count-input').val(countValue);

            var resultValue = countValue*sumValue;

            $('.shop-product__product-sum').val(resultValue);

        }

        if(countValue===0) {
            $(this).parent().parent().parent().find('.shop-product__steps-checkmark').removeClass('checked');
        }

    });

    $('.shop-product__count-btn--plus').click(function () {

        if (countValue > -1) {
            $(this).parent().parent().parent().find('.shop-product__steps-checkmark').addClass('checked');
        }

        if (countValue<20) {

            countValue++;

            $('.shop-product__count-input').val(countValue);

            var resultValue = countValue*sumValue;

            $('.shop-product__product-sum').val(resultValue);

        }

    });

    // end


    // video modals action

    $('.android__get-xibo__play-video-icon, .header__play-video-icon').click(function () {
       $('.video-player-modal').fadeIn('300');

    });

    $('.close-video-player').click(function () {
        $('.video-player-modal').fadeOut('300');
    });

    // end


    // calculate form

    $('.hosting__calculate__checkboxes input[type=checkbox]').each(function() {
        if ($(this).is(':checked')) {
            $(this).parent().addClass('checked');
            var value = $(this).parent().find('span').text();
            $('.hosting__calculate__sum').val(value);
        }
    });

    $('.hosting__calculate__checkboxes input[type=checkbox]').click(function () {

        if($(this).is(':checked')) {
            $(this).parent().addClass('checked');
            var value = $(this).parent().find('span').text();
            $('.hosting__calculate__sum').val(value);
        } else {
            $(this).parent().removeClass('checked');
            var value = '';
            $('.hosting__calculate__sum').val(value);
        }

    });

    // end


    // accordion

    $('.panel-heading').click(function () {

        var thisItemParent = $(this).parent();

        $(this).parent().toggleClass('accordion-shadowed');

        $('#accordion.panel-group').find('.accordion-shadowed').not(thisItemParent).removeClass('accordion-shadowed');

    });

    $('#accordion.panel-group').find('.panel.panel-default:first').addClass('accordion-shadowed');

    $('.panel-heading a').append(
        "<img src='img/svg/Help/icon_help_arrow-down.svg'>" +
        "<img src='img/svg/Help/icon_help_close.svg'>"
    );

    // end


    // img equal to box

    // var equalImg = function () {
    //
    //     $('.design__portfolio img').each(function() {
    //
    //         var imgHeight = $(this).height();
    //
    //         var imgWidth = $(this).width();
    //
    //         var imgWHDifference = imgWidth/imgHeight;
    //
    //         var imgParentHeight = $(this).parent().height();
    //
    //         var imgParentWidth = $(this).parent().width();
    //
    //         if (imgParentHeight>imgHeight) {
    //
    //             $(this).css({
    //                 'width' : imgWidth*imgWHDifference
    //             });
    //
    //             imgHeight = imgHeight * imgWHDifference;
    //
    //             imgWidth = imgWidth*imgWHDifference;
    //
    //         }
    //
    //         var widthResult = (imgWidth-imgParentWidth)/2;
    //
    //         var heightResult = (imgHeight-imgParentHeight)/2;
    //
    //         $(this).css({
    //             'right' : widthResult,
    //             'bottom' : heightResult
    //         });
    //     });
    //
    // };
    //
    // equalImg();

    // end


    // popup in download page

    var popupOpened = false;

    $('.download .popup-link').click(function (e) {

        e.preventDefault();

        if (!popupOpened) {

            $(this).parent().parent().parent().find('.download__popup').show();

            setTimeout(function () {
                popupOpened = true;
            });

        }


    });

    $('.download').click(function () {

        if (popupOpened) {

           $('.download__popup').hide();

           popupOpened = false;
        }

    });

    // end


    // about page tabs


    $('.about__config__tabs .cms-btn').click(function () {
        $('.container.about__config__container').css({
            'max-width' : '980px'
        })
    });

    $('.about__config__tabs .signage-btn').click(function () {
        $('.container.about__config__container').css({
            'max-width' : '1170px'
        })
    })

    // end


});