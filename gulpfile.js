var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    prefix = require('gulp-autoprefixer'),
    minifycss = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    svgmin = require('gulp-svgmin'),
    imagemin = require('gulp-imagemin'),
    size = require('gulp-size'),
    pug = require('gulp-pug'),
    bs = require('browser-sync');

gulp.task('browser-sync', ['sass'], function() {
   bs.init({
       server: {
           baseDir: './dist'
       }
   });
});

gulp.task('sass', function() {
    return sass('./src/sass/**/*.sass')
        .pipe(prefix('last 16 versions'))
        .pipe(minifycss({ compatibility: 'ie8' }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./dist/css'))
        .pipe(bs.reload({ stream: true }));
});


gulp.task('pug', function() {
    return gulp.src('./src/*.pug')
        .pipe(pug({ pretty: true }))
        .pipe(gulp.dest('./dist'))
        .pipe(bs.reload({ stream: true }));
});

gulp.task('js', function() {
    return gulp.src('./src/js/*.js')
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./dist/js'))
        .pipe(bs.reload({ stream: true }));
});

gulp.task('image', function() {
    return gulp.src('./src/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/img'));
});

gulp.task('svg', function() {
    return gulp.src('./src/img/svg/**/*.svg')
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/img/svg'));
});

gulp.task('fonts', function() {
    return gulp.src('./src/fonts/**/*.ttf')
        .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('stats', function() {
    return gulp.src('./dist/**/*')
        .pipe(size())
        .pipe(gulp.dest('./dist'));
});

gulp.task('default', ['browser-sync'], function() {
    gulp.watch('./src/sass/**/*.sass', ['sass']);
    gulp.watch('./src/*.pug', ['pug']);
    gulp.watch('./src/js/*.js', ['js']);
    gulp.watch('./src/img/**/*', ['image', 'svg']);
    gulp.watch('./src/fonts/**/*.ttf', ['fonts']);
    gulp.watch('./dist/*.html').on('change', bs.reload);
});
